from django.http import HttpResponse, HttpResponseRedirect
from django.template.loader import render_to_string
import json
import requests

HEADERS = { 'User-Agent' : 'spam/1.0' }


def rickroll(request):
    #return HttpResponse("Never gonna give you up")
    return HttpResponseRedirect("https://www.youtube.com/watch?v=dQw4w9WgXcQ")


def fetch_advice_urls():
    d = json.loads(requests.get("http://reddit.com/r/adviceanimals/.json", headers=HEADERS).text)
    urls = [c['data']['url'] for c in d['data']['children']]
    return list(filter(lambda u: u.endswith(".jpg"), urls))


def advice(request):    
    img_urls = fetch_advice_urls()
    body = render_to_string('advice.html', {'img_urls' : img_urls})
    return HttpResponse(body)


def advice_json(request):
    return HttpResponse(json.dumps(dict(urls=fetch_advice_urls())),
            content_type="application/json")

       

